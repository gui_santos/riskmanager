﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RiskManager
{
    class Program
    {
        static void Main(string[] args)
        {
            IUnitOfWork unitOfWork = new UnitOfWork();
            ISBUnitOfWork sbUnitofWork = new SBUnitOfWork();
            var data = new DateTime(2017, 06, 14);
            var holidays = new Holidays().HolidayDates;
            var CDI_Curve = File.ReadAllLines(Global.InterpolatedCDIPath(data))
                               .Select(v => InterpolatedCDI.FromCsv(v))
                               .ToList();

            var dadosCessao = unitOfWork.AssignmentRepository.GetAssignmentData(data);
            var dadosAntecipacao = unitOfWork.PrePaymentRepository.GetPrePaymentData(data);

            var assignmentRisk = dadosCessao.Select(x => RiskItem.AssignmentDataToRiskItem(CDI_Curve.First(i => i.ConsecutiveDays == (x.DT_VENCIMENTO - data).Days), data, x, holidays)).ToList();
            var prePaymentRisk = dadosAntecipacao.Select(x => RiskItem.PrePaymentToRiskItem(CDI_Curve.First(i => i.ConsecutiveDays == (x.DT_ORIGINAL_DUE_DATE - data).Days), data, x, holidays)).ToList();

            var div01Cessao = assignmentRisk.Sum(x => x.Delta);
            var div01Antecipacao = prePaymentRisk.Sum(x => x.Delta);
            var NET_IR_Exposure = div01Antecipacao - div01Cessao;
            var DIV01_1_Tri = prePaymentRisk.Where(x => x.Trimestre == 1).Sum(x => x.Delta) - assignmentRisk.Where(x => x.Trimestre == 1).Sum(x => x.Delta);
            var DIV01_2_Tri = prePaymentRisk.Where(x => x.Trimestre == 2).Sum(x => x.Delta) - assignmentRisk.Where(x => x.Trimestre == 2).Sum(x => x.Delta);
            var DIV01_3_Tri = prePaymentRisk.Where(x => x.Trimestre == 3).Sum(x => x.Delta) - assignmentRisk.Where(x => x.Trimestre == 3).Sum(x => x.Delta);
            var DIV01_4_Tri = prePaymentRisk.Where(x => x.Trimestre == 4).Sum(x => x.Delta) - assignmentRisk.Where(x => x.Trimestre == 4).Sum(x => x.Delta);

            var interestRateRiskPosition = new InterestRatesRiskPosition
            {
                positionDate = data,
                DIV01_Cessao = div01Cessao,
                DIV01_Antecipacao = div01Antecipacao,
                NET_IR_Exposure = NET_IR_Exposure,
                DIV01_1_Tri = DIV01_1_Tri,
                DIV01_2_Tri = DIV01_2_Tri,
                DIV01_3_Tri = DIV01_3_Tri,
                DIV01_4_Tri = DIV01_4_Tri,
            };


            var ScenariosList = unitOfWork.StressTestScenarioRepository.GetStressTestScenarios();

            var stressTestResults = ScenariosList.Select(x => x.ApplySTScenarios(interestRateRiskPosition));

            sbUnitofWork.StressTestSituationRepository.AddRange(stressTestResults.ToList());
            sbUnitofWork.Commit();
            //var stressTestScenario1 = new StressTestScenarioOld(StressTestEnumOld.parallelShockUp);
            //var stressTestScenario2 = new StressTestScenarioOld(StressTestEnumOld.parallelShockDown);
            //var stressTestScenario3 = new StressTestScenarioOld(StressTestEnumOld.steepenerShock);
            //var stressTestScenario4 = new StressTestScenarioOld(StressTestEnumOld.flattenerShock);
            //var stressTestScenario5 = new StressTestScenarioOld(StressTestEnumOld.shortRatesShockUp);
            //var stressTestScenario6 = new StressTestScenarioOld(StressTestEnumOld.shortRatesShockDown);

            //var ST1 = stressTestScenario1.ApplySTScenarios(interestRateRiskPosition);
            //var ST2 = stressTestScenario2.ApplySTScenarios(interestRateRiskPosition);
            //var ST3 = stressTestScenario3.ApplySTScenarios(interestRateRiskPosition);
            //var ST4 = stressTestScenario4.ApplySTScenarios(interestRateRiskPosition);
            //var ST5 = stressTestScenario5.ApplySTScenarios(interestRateRiskPosition);
            //var ST6 = stressTestScenario6.ApplySTScenarios(interestRateRiskPosition);

            //var STPosition = new StressTestPositionOld
            //{
            //    positionDate = data,
            //    ST1 = ST1,
            //    ST2 = ST2,
            //    ST3 = ST3,
            //    ST4 = ST4,
            //    ST5 = ST5,
            //    ST6 = ST6,
            //};

            Console.WriteLine($"positionDate: {interestRateRiskPosition.positionDate} \n\r");
            Console.WriteLine($"DIV01_Antecipacao: {interestRateRiskPosition.DIV01_Antecipacao} \n\r");
            Console.WriteLine($"DIV01_Cessao: {interestRateRiskPosition.DIV01_Cessao} \n\r");
            Console.WriteLine($"NET_IR_Exposure: {interestRateRiskPosition.NET_IR_Exposure} \n\r");
            Console.WriteLine($"DIV01_1_Tri: {interestRateRiskPosition.DIV01_1_Tri} \n\r");
            Console.WriteLine($"DIV01_2_Tri: {interestRateRiskPosition.DIV01_2_Tri} \n\r");
            Console.WriteLine($"DIV01_3_Tri: {interestRateRiskPosition.DIV01_3_Tri} \n\r");
            Console.WriteLine($"DIV01_4_Tri {interestRateRiskPosition.DIV01_4_Tri} \n\r");
            Console.WriteLine($"ST2: {stressTestResults.First(x => x.scenarioId == 1).PnLImpact}\n\r");
            Console.WriteLine($"ST3: {stressTestResults.First(x => x.scenarioId == 2).PnLImpact}\n\r");
            Console.WriteLine($"ST4: {stressTestResults.First(x => x.scenarioId == 3).PnLImpact}\n\r");
            Console.WriteLine($"ST5: {stressTestResults.First(x => x.scenarioId == 4).PnLImpact}\n\r");
            Console.WriteLine($"ST6: {stressTestResults.First(x => x.scenarioId == 5).PnLImpact}\n\r");
            Console.WriteLine($"ST6: {stressTestResults.First(x => x.scenarioId == 6).PnLImpact}\n\r");
            //Console.WriteLine("Cessão");
            //assignmentRisk.OrderBy(i => i.DU).ToList().ForEach(i => Console.Write($"{i.CDI} | {Global.WorkdaysAdd(data,i.DU,holidays).Date} | {i.NPV} | {i.Trimestre} | {i.Delta} \n\r"));
            //Console.WriteLine("Antecipação");
            //prePaymentRisk.OrderBy(i => i.DU).ToList().ForEach(i => Console.Write($"{i.CDI} | {Global.WorkdaysAdd(data, i.DU, holidays).Date} | {i.NPV} | {i.Trimestre} | {i.Delta} \n\r"));
            //Console.ReadLine();
            //Console.WriteLine("Antecipacao");
            //dadosAntecipacao.OrderBy(i => i.DT_ORIGINAL_DUE_DATE).ToList().ForEach(i => Console.Write($"{i.DT_ORIGINAL_DUE_DATE.ToString("dd/MM/yyyy")} | {i.VL_ANTECIPADO} \n\r"));
            //Console.WriteLine("Cessao");
            //dadosCessao.OrderBy(i => i.DT_VENCIMENTO).ToList().ForEach(i => Console.Write($"{i.DT_VENCIMENTO.ToString("dd/MM/yyyy")} | {i.VL_CESSAO} \n\r"));
            //Console.WriteLine("DI");
            ////CDI_Curve.OrderBy(i => i.ConsecutiveDays).ToList().ForEach(i => Console.Write($"{i.vertice}|{i.ConsecutiveDays}|{i.InterpolatedTx_252}|{i.Tx_360}|{i.InterpolatedOver_252}|{i.Over_360}\n\r", i));
            //Console.ReadLine();

            Console.ReadLine();
            sbUnitofWork.Dispose();
        }
    }
}
