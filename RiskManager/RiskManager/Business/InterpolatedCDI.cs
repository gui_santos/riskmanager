﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RiskManager
{
    class InterpolatedCDI
    {
        public int vertice { get; set; }
        public int ConsecutiveDays{ get; set; }
        public decimal InterpolatedTx_252 { get; set; }
        public decimal Tx_360 { get; set; }
        public decimal InterpolatedOver_252 { get; set; }
        public decimal Over_360 { get; set; }

        public static InterpolatedCDI FromCsv(string csvLine)
        {
            var dados = Array.ConvertAll(csvLine.Split(';'), double.Parse);
            
            var CDI_Item = new InterpolatedCDI()
            {
                vertice = (int)dados[0],
                ConsecutiveDays = (int)dados[1],
                InterpolatedTx_252 = (decimal)dados[2]/100,
                Tx_360 = (decimal)dados[3],
                InterpolatedOver_252 = (decimal)dados[4],
                Over_360 = (decimal)dados[5],                
            };

            return CDI_Item;
        }
    }
}
