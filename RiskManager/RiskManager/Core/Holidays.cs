﻿using Dlp.Connectors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RiskManager
{
    class Holidays
    {
        public List<DateTime> HolidayDates { get; set; }

        public Holidays()
        {
            HolidayDates = new List<DateTime>();
            var sandboxConnectionString = Global.ConnectionString;
            var sandboxDatabaseConnector = new DatabaseConnector(sandboxConnectionString, 600);
            var query = @"SELECT [Date] FROM [BD_FINANCEIRO].[dbo].[Holidays]";

            HolidayDates = sandboxDatabaseConnector.ExecuteReader<DateTime>(query, new { }).ToList();
            sandboxDatabaseConnector.Close();
        }
    }
}
