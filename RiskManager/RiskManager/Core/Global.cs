﻿using RiskManager.Properties;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RiskManager
{
    class Global
    {
        static Global() { ConnectionString = Resources.ConnectionString; } // default value

        public static string ConnectionString { get; private set; }

        public static string InterpolatedCDIPath(DateTime data)
        {
            var CashFlowDir = $@"\\172.16.104.20\Shared\Financial\Tesouraria\Curva Controle de Liquidez\Relatorios\{data.ToString("yyyMMdd")}\Predictions\";
            Directory.Exists(CashFlowDir);
            var filenames = new List<string>();
            filenames.AddRange(Directory.GetFiles(CashFlowDir));

            var InterpolatedCDIPath = filenames.First(i => i.Contains("InterpolatedCDI-BMF"));

            return InterpolatedCDIPath;
        }

        public static int NetWorkDays(DateTime firstDate, DateTime lastDate, List<DateTime> holidays)
        {

            if (firstDate > lastDate)// Swap the dates if firstDate > lastDate
            {
                var tempDate = firstDate;
                firstDate = lastDate;
                lastDate = tempDate;
            }
            var days = (int)(lastDate.Subtract(firstDate).Ticks / TimeSpan.TicksPerDay);
            var weekReminder = days % 7;
            if (weekReminder > 0)
            {
                switch (firstDate.DayOfWeek)
                {
                    case DayOfWeek.Monday:
                        days = days - ((weekReminder > 5) ? 1 : 0);
                        // Another way for this:
                        //days = days - ((int)weekReminder % 5);
                        // but i think its more expensive
                        break;
                    case DayOfWeek.Tuesday:
                        days = days - ((weekReminder > 4) ? 1 : 0) - ((weekReminder > 5) ? 1 : 0);
                        // The same from above
                        //days = days - ((int)weekReminder % 4);
                        break;
                    case DayOfWeek.Wednesday:
                        days = days - ((weekReminder > 3) ? 1 : 0) - ((weekReminder > 4) ? 1 : 0);
                        break;
                    case DayOfWeek.Thursday:
                        days = days - ((weekReminder > 2) ? 1 : 0) - ((weekReminder > 3) ? 1 : 0);
                        break;
                    case DayOfWeek.Friday:
                        days = days - ((weekReminder > 1) ? 1 : 0) - ((weekReminder > 2) ? 1 : 0);
                        break;
                    case DayOfWeek.Saturday:
                        days = days - 1 - ((weekReminder > 1) ? 1 : 0);
                        break;
                    case DayOfWeek.Sunday:
                        days = days - 1;
                        break;
                }
            }
            days = days - (2 * ((int)days / 7));
            if (holidays != null && holidays.Count() > 0)
            {
                foreach (DateTime holiday in holidays.Where(
                          h => h >= firstDate && h < lastDate))
                {
                    var dayOfWeekOfHoliday = holiday.DayOfWeek;
                    if (dayOfWeekOfHoliday != DayOfWeek.Saturday &&
                        dayOfWeekOfHoliday != DayOfWeek.Sunday)
                    {
                        days = days - 1;
                    }
                }
            }
            return days;
        }

        public static DateTime AddWorkday(DateTime date, List<DateTime> HolidayDates, int i)
        {
            var ret = date.AddDays(i);
            if (HolidayDates.Any(d => d.Year == ret.Year && d.Month == ret.Month && d.Day == ret.Day) || ret.DayOfWeek == DayOfWeek.Saturday || ret.DayOfWeek == DayOfWeek.Sunday)
                return AddWorkday(ret, HolidayDates, i);

            else
                return ret;
        }

        public static DateTime WorkdaysAdd(DateTime date, int datediff, List<DateTime> holidays)
        {
            DateTime newDate;
            newDate = date;

            for (int i = 0; i < (int)Math.Abs(datediff); i++)
            {
                newDate = AddWorkday(newDate, holidays, (datediff / (int)Math.Abs(datediff)));
            }
            return newDate;
        }
    }
}
