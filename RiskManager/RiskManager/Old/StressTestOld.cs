﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RiskManager
{
    class StressTestScenarioOld
    {
        public decimal FirstQuarter { get; set; }
        public decimal SecondQuarter { get; set; }
        public decimal ThirdQuarter { get; set; }
        public decimal FourthQuarter { get; set; }

        public StressTestScenarioOld(StressTestEnumOld STScenario)
        {
            //switch((int)STScenario)
            //{
            //    case 1:
            //        FirstQuarter = 150;
            //        SecondQuarter = 150;
            //        ThirdQuarter = 150;
            //        FourthQuarter = 150;
            //        break;

            //    case 2:
            //        FirstQuarter = -150;
            //        SecondQuarter = -150;
            //        ThirdQuarter = -150;
            //        FourthQuarter = -150;
            //        break;

            //    case 3:
            //        FirstQuarter = -150;
            //        SecondQuarter = -150;
            //        ThirdQuarter = 150;
            //        FourthQuarter = 150;
            //        break;

            //    case 4:
            //        FirstQuarter = 150;
            //        SecondQuarter = 150;
            //        ThirdQuarter = -150;
            //        FourthQuarter = -150;
            //        break;

            //    case 5:
            //        FirstQuarter = 150;
            //        SecondQuarter = 150;
            //        ThirdQuarter = 0;
            //        FourthQuarter = 0;
            //        break;

            //    case 6:
            //        FirstQuarter = -150;
            //        SecondQuarter = -150;
            //        ThirdQuarter = 0;
            //        FourthQuarter = 0;
            //        break;

            //    default:
            //        FirstQuarter = 0;
            //        SecondQuarter = 0;
            //        ThirdQuarter = 0;
            //        FourthQuarter = 0;
            //        break;
            //}


        }

        //public StressTestScenario(int _firstQuarter, int _secondQuarter, int _thirdQuarter, int _fourthQuarter)
        //{
        //    FirstQuarter = _firstQuarter;
        //    SecondQuarter = _secondQuarter;
        //    ThirdQuarter = _thirdQuarter;
        //    FourthQuarter = _fourthQuarter;
        //}

        public decimal ApplySTScenarios(InterestRatesRiskPosition risk)
        {
            decimal PnLImpact = 0;

            PnLImpact += risk.DIV01_1_Tri * (-FirstQuarter);
            PnLImpact += risk.DIV01_2_Tri * (-SecondQuarter);
            PnLImpact += risk.DIV01_3_Tri * (-ThirdQuarter);
            PnLImpact += risk.DIV01_4_Tri * (-FourthQuarter);
            return PnLImpact;
        }
    }
}
