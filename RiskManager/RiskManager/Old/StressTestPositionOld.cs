﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RiskManager
{
    class StressTestPositionOld
    {
        public DateTime positionDate { get; set; }
        public decimal ST1 { get; set; }
        public decimal ST2 { get; set; }
        public decimal ST3 { get; set; }
        public decimal ST4 { get; set; }
        public decimal ST5 { get; set; }
        public decimal ST6 { get; set; }
    }
}
