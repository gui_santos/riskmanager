﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RiskManager
{
    public enum StressTestEnumOld
    {
        parallelShockUp = 1,
        parallelShockDown = 2,
        steepenerShock = 3,
        flattenerShock = 4,
        shortRatesShockUp = 5,
        shortRatesShockDown = 6
    }
}
