﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RiskManager
{
    [Table("StressTestPnLImpact", Schema = "dbo")]
    public class StressTestSituation
    {
        [Key]
        public int Id { get; set; }
        public int scenarioId { get; set; }
        public DateTime positionDate { get; set; }
        public decimal PnLImpact { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime CreatedAt { get; set; }
    }
}
