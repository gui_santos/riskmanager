﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RiskManager
{
    [Table("StressTestScenarios", Schema = "dbo")]
    public class StressTestScenario
    {
        [Key]
        public int Id { get; set; }
        private decimal FirstQuarterShock { get; set; }
        private decimal SecondQuarterShock { get; set; }
        private decimal ThirdQuarterShock { get; set; }
        private decimal FourthQuarterShock { get; set; }

        public StressTestSituation ApplySTScenarios(InterestRatesRiskPosition risk)
        {

            decimal PnLImpact = 0;

            PnLImpact += risk.DIV01_1_Tri * (-FirstQuarterShock);
            PnLImpact += risk.DIV01_2_Tri * (-SecondQuarterShock);
            PnLImpact += risk.DIV01_3_Tri * (-ThirdQuarterShock);
            PnLImpact += risk.DIV01_4_Tri * (-FourthQuarterShock);

            return new StressTestSituation
            {
                scenarioId = Id,
                positionDate = risk.positionDate,
                PnLImpact = PnLImpact
            };
        }
    }
}


