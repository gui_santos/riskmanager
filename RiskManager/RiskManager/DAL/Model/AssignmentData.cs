﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RiskManager
{
    [Table("TBSTONEF_DADOS_CESSAO", Schema = "dbo")]
    public class AssignmentData
    {   
        [Key]
        [Column("DT_VENCIMENTO")]
        public DateTime DT_VENCIMENTO { get; set; }
        [Column("VL_CESSAO")]
        public Decimal VL_CESSAO { get; set; }

    }
}
