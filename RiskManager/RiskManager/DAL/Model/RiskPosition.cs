﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RiskManager
{
    public class InterestRatesRiskPosition
    {
        public DateTime positionDate { get; set; }
        public decimal DIV01_Cessao { get; set; }
        public decimal DIV01_Antecipacao { get; set; }
        public decimal NET_IR_Exposure { get; set; }
        public decimal DIV01_1_Tri { get; set; }
        public decimal DIV01_2_Tri { get; set; }
        public decimal DIV01_3_Tri { get; set; }
        public decimal DIV01_4_Tri { get; set; }
    }
}
