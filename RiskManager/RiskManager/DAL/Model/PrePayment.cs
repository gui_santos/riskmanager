﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RiskManager
{
    [Table("TBSTONEF_ANTECIPACAO_D0", Schema = "dbo")]
    public class PrePayment
    {
        [Key]
        [Column("DT_ORIGINAL_DUE_DATE")]
        public DateTime DT_ORIGINAL_DUE_DATE { get; set; }
        [Column("VL_ANTECIPADO")]
        public decimal VL_ANTECIPADO { get; set; }
    }
}
