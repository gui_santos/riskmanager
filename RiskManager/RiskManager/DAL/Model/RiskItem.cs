﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RiskManager
{
    class RiskItem
    {
        public decimal CDI { get; set; }
        public int DU { get; set; }
        public decimal NPV { get; set; }
        public int Trimestre { get; set; }
        public decimal Delta { get; set; }

        public static RiskItem PrePaymentToRiskItem(InterpolatedCDI CDI, DateTime data, PrePayment prePaymentItem, List<DateTime> holidays)
        {
            
            var diasUteis = Global.NetWorkDays(data, prePaymentItem.DT_ORIGINAL_DUE_DATE, holidays);
            var trimestre = Math.Ceiling((prePaymentItem.DT_ORIGINAL_DUE_DATE - data).Days / 90.0);
            var npv = prePaymentItem.VL_ANTECIPADO * (decimal)Math.Pow((double)((decimal)1.0 + CDI.InterpolatedTx_252), (-1 * (diasUteis / 252.0)));
           // Console.WriteLine($"{CDI.ConsecutiveDays} | {CDI.InterpolatedTx_252} | {data} | {prePaymentItem.DT_ORIGINAL_DUE_DATE} | {prePaymentItem.VL_ANTECIPADO} | {diasUteis} | {npv}");
            var riskItem = new RiskItem
            {
                CDI = CDI.InterpolatedOver_252,
                DU = diasUteis,
                NPV = npv,
                Delta = prePaymentItem.VL_ANTECIPADO * (decimal)Math.Pow((double)((decimal)(1.0 - 0.0001) + CDI.InterpolatedTx_252), (-1 * (diasUteis / 252.0))) - npv,
                Trimestre = (int)Math.Ceiling((decimal)trimestre),
            };

            return riskItem;
        }

    public static RiskItem AssignmentDataToRiskItem(InterpolatedCDI CDI, DateTime data, AssignmentData assignmentItem, List<DateTime> holidays)
    {
            
            var diasUteis = Global.NetWorkDays(data, assignmentItem.DT_VENCIMENTO, holidays);
            var trimestre = Math.Ceiling((assignmentItem.DT_VENCIMENTO - data).Days / 90.0);
            var npv = assignmentItem.VL_CESSAO * (decimal)Math.Pow((double)((decimal)1.0 + CDI.InterpolatedTx_252), (-1*(diasUteis / 252.0)));
            //Console.WriteLine($"{CDI.ConsecutiveDays} | {CDI.InterpolatedTx_252} | {data} | {assignmentItem.DT_VENCIMENTO} | {assignmentItem.VL_CESSAO} | {diasUteis} | {npv}");
            var riskItem = new RiskItem
            {
                CDI = CDI.InterpolatedOver_252,
                DU = diasUteis,
                NPV = npv,
                Delta = (assignmentItem.VL_CESSAO * (decimal)Math.Pow((double)((decimal)(1.0 - 0.0001) + CDI.InterpolatedTx_252), (-1 * (diasUteis / 252.0)))) - npv,
                Trimestre = (int)Math.Ceiling((decimal)trimestre),
            };

            return riskItem;
    }
}
}
