﻿using Dlp.Buy4.Repository.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RiskManager
{
    public interface IStressTestScenarioRepository : IRepository<StressTestScenario>
    {
        List<StressTestScenario> GetStressTestScenarios();
    }
}
