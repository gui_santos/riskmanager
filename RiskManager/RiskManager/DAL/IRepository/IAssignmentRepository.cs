﻿using Dlp.Buy4.Repository.Base;
using System;
using System.Collections.Generic;

namespace RiskManager
{
    public interface IAssignmentDataRepository : IRepository<AssignmentData>
    {
        List<AssignmentData> GetAssignmentData(DateTime data);
    }
}