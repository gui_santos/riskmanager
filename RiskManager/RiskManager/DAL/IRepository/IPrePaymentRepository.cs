﻿using Dlp.Buy4.Repository.Base;
using System;
using System.Collections.Generic;

namespace RiskManager
{
    public interface IPrePaymentRepository : IRepository<PrePayment>
    {
        List<PrePayment> GetPrePaymentData(DateTime data);
    }
}