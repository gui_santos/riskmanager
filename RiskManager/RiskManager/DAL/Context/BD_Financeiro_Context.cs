﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RiskManager
{
    public class BDFinanceiroContext : DbContext
    {
        /// <summary>
        /// Construtor
        /// </summary>
        public BDFinanceiroContext() : base("name=BD_FINANCEIRO")
        {
            // Seta o inicializador para fazer nada quando conectado com banco de dados
            // A configuração padrão é recriação dos objetos do banco de dados caso o ORM detecte alguma diferença entre o contexto de entidades modelo e banco de dados
            Database.SetInitializer(new NullDatabaseInitializer<BDFinanceiroContext>());

            // Setar essa configuração como falso melhora a perfomance do ORM uma vez que o entity framework não necessite procurar 
            // por alterações entre o contexto de entidades modelo e banco de dados
            // Entretanto, para alterar um objeto é necessário setar o entry da entidade como 'Modified'
            base.Configuration.AutoDetectChangesEnabled = false;

            // Seta timeout in segundos dos comandos de execução do ORM
            ((IObjectContextAdapter)this).ObjectContext.CommandTimeout = 1800;
        }

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="connectionString">Connectionstring para conectar no banco de dados</param>
        private BDFinanceiroContext(string connectionString) : base(connectionString) { }

        /// <summary>
        /// Método OnModelCreating 
        /// </summary>
        /// <param name="modelBuilder">Objeto DbModelBuilder</param>
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            // Configuração padrão do ORM
            base.OnModelCreating(modelBuilder);

            // Remove convenção de pluralização (nomes de tabelas pluralizadas em queries geradas automaticamente).
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }

        public DbSet<AssignmentData> AssignmentData { get; set; }
        public DbSet<PrePayment> PrePayment { get; set; }
    }
}
