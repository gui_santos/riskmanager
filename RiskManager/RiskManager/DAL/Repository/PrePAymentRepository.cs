﻿using Dlp.Buy4.Repository.Base;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RiskManager
{
    public sealed class PrePaymentRepository : Repository<PrePayment>, IPrePaymentRepository
    {
        Database _database;

        public PrePaymentRepository(BDFinanceiroContext dbContext) : base(dbContext)
        {
            this._database = dbContext.Database;
        }

        public List<PrePayment> GetPrePaymentData(DateTime data)
        {
            var sqlPrepayment = $@"Select A.DT_ORIGINAL_DUE_DATE
		                                    ,Sum(A.[VL_ANTECIPADO]) VL_ANTECIPADO
                                    from(SELECT DateAdd(d,[NR_DURATION], [DT_ANTECIPACAO]) as DT_ORIGINAL_DUE_DATE
                                          ,[VL_NET_AMOUNT] VL_ANTECIPADO
                                          ,[DS_TIPO_ANTECIPACAO] DS_TIPO_ANTECIPACAO
                                      FROM [BDMDMIS_STONE].[dbo].[TBSTONEF_ANTECIPACAO_RECEBIVEL] (nolock)
                                      where [DT_ANTECIPACAO] <=  '{data.ToString("yyyy-MM-dd")}') A
                                      Where A.DT_ORIGINAL_DUE_DATE > '{data.ToString("yyyy-MM-dd")}'
                                      Group by A.DT_ORIGINAL_DUE_DATE
                                      Order by A.DT_ORIGINAL_DUE_DATE asc";
            try
            {
                var agreementsList = this._database.SqlQuery<PrePayment>(sqlPrepayment).ToList();
                return agreementsList;
            }
            catch (Exception e)
            {
                Console.WriteLine("++++ ERROR SQL : " + e.Message);
            }
            return new List<PrePayment>();
        }
    }
}
