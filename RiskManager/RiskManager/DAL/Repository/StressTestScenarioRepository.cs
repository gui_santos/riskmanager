﻿using Dlp.Buy4.Repository.Base;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RiskManager
{

    public sealed class StressTestScenarioRepository : Repository<StressTestScenario>, IStressTestScenarioRepository
    {
        Database _database;

        public StressTestScenarioRepository(BDFinanceiroContext dbContext) : base(dbContext)
        {
            this._database = dbContext.Database;
        }

        public List<StressTestScenario> GetStressTestScenarios()
        {
            var sqlPrepayment = $@"SELECT [Id]
                          ,[FirstQuarterShock]
                          ,[SecondQuarterShock]
                          ,[ThirdQuarterShock]
                          ,[FourthQuarterShock]
                      FROM [DB_SANDBOX].[dbo].[StressTestScenarios]";
            try
            {
                var agreementsList = this._database.SqlQuery<StressTestScenario>(sqlPrepayment).ToList();
                return agreementsList;
            }
            catch (Exception e)
            {
                Console.WriteLine("++++ ERROR SQL : " + e.Message);
            }
            return new List<StressTestScenario>();
        }
    } 
}
