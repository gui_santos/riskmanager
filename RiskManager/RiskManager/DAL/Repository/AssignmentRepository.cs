﻿using Dlp.Buy4.Repository.Base;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RiskManager
{
    public sealed class AssignmentRepository : Repository<AssignmentData>, IAssignmentDataRepository
    {
        Database _database;

        public AssignmentRepository(BDFinanceiroContext dbContext) : base(dbContext)
        {
            this._database = dbContext.Database;
        }

        public List<AssignmentData> GetAssignmentData(DateTime data)
        {
            var sqlAssignment = $@"SELECT [DT_VENCIMENTO] As DT_VENCIMENTO
                                    ,SUM([VL_CESSAO]) AS VL_CESSAO
                                FROM [BD_FINANCEIRO].[dbo].[TBSTONEF_DADOS_CESSAO]
                                Where DT_VENCIMENTO > '{data.ToString("yyyy-MM-dd")}'
                                AND [DT_CESSAO] <= '{data.ToString("yyyy-MM-dd")}'
                                group by DT_VENCIMENTO
                                order by DT_VENCIMENTO";
            try
            {
                var agreementsList = this._database.SqlQuery<AssignmentData>(sqlAssignment).ToList();
                return agreementsList;
            }
            catch (Exception e)
            {
                Console.WriteLine("++++ ERROR SQL : " + e.Message);
            }
            return new List<AssignmentData>();
        }
    }
}
