﻿using System;

namespace RiskManager
{
    public interface ISBUnitOfWork : IDisposable
    {
        IStressTestSituationRepository StressTestSituationRepository { get; }
        void Commit();
    }
}