﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RiskManager
{
    public sealed class UnitOfWork : IUnitOfWork
    {
        bool _disposed = false;

        private readonly BDFinanceiroContext _backOfficeContext;

        public UnitOfWork(BDFinanceiroContext backofficeContext = null)
        {
            if (_backOfficeContext == null)
            {
                _backOfficeContext = new BDFinanceiroContext();
            }
            else
            {
                _backOfficeContext = backofficeContext;
            }
        }

        private IAssignmentDataRepository _assignmentRepository;
        public IAssignmentDataRepository AssignmentRepository
        {
            get { return _assignmentRepository ?? (this._assignmentRepository = new AssignmentRepository(this._backOfficeContext)); }
        }

        private IPrePaymentRepository _prePaymentRepository;
        public IPrePaymentRepository PrePaymentRepository
        {
            get { return _prePaymentRepository ?? (this._prePaymentRepository = new PrePaymentRepository(this._backOfficeContext)); }
        }

        private IStressTestScenarioRepository _stressTestScenarioRepository;
        public IStressTestScenarioRepository StressTestScenarioRepository
        {
            get { return _stressTestScenarioRepository ?? (this._stressTestScenarioRepository = new StressTestScenarioRepository(this._backOfficeContext)); }
        }

        public void Commit()
        {
            _backOfficeContext.SaveChanges();
        }

        public void Dispose()
        {
            Dispose(true);

            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (_disposed) return;

            if (disposing)
            {
                if (_disposed) return;

                _backOfficeContext.Dispose();

                _disposed = true;
            }

            _disposed = true;
        }
    }
}
