﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RiskManager
{
    public sealed class SBUnitOfWork : ISBUnitOfWork
    {
        bool _disposed = false;

        private readonly BD_SandboxContext _backOfficeContext;

        public SBUnitOfWork(BD_SandboxContext backofficeContext = null)
        {
            if (_backOfficeContext == null)
            {
                _backOfficeContext = new BD_SandboxContext();
            }
            else
            {
                _backOfficeContext = backofficeContext;
            }
        }

        private IStressTestSituationRepository _stressTestSituationRepository;
        public IStressTestSituationRepository StressTestSituationRepository
        {
            get { return _stressTestSituationRepository ?? (this._stressTestSituationRepository = new StressTestSituationRepository(this._backOfficeContext)); }
        }

        public void Commit()
        {
            _backOfficeContext.SaveChanges();
        }

        public void Dispose()
        {
            Dispose(true);

            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (_disposed) return;

            if (disposing)
            {
                if (_disposed) return;

                _backOfficeContext.Dispose();

                _disposed = true;
            }

            _disposed = true;
        }
    }
}
