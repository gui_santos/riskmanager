﻿namespace RiskManager
{
    public interface IUnitOfWork
    {
        IAssignmentDataRepository AssignmentRepository { get; }
        IPrePaymentRepository PrePaymentRepository { get; }
        IStressTestScenarioRepository StressTestScenarioRepository { get; }
    }
}